
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE DodajKonto 
	-- Add the parameters for the stored procedure here
	@id_oddzialu numeric(18, 0), 
	@id_klienta numeric(18, 0),
	@stan float,
	@waluta char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	insert into Konto (id_oddzialu, id_klienta, stan, waluta) values (@id_oddzialu, @id_klienta, @stan, @waluta)

    -- Insert statements for procedure here
	SELECT @id_oddzialu, @id_klienta
END
GO
