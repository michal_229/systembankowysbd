create trigger Bankomat_aiu
on Bankomat
after insert, update 
as
begin
	-- nie da sie dobrac do pola adres : text

	declare @id_bankomatu numeric(18, 0) 
	declare @id_oddzialu numeric(18, 0) 

	select @id_bankomatu = id_bankomatu, @id_oddzialu = id_oddzialu from inserted

	--if @id_oddzialu = 13 begin
	--	delete from Bankomat where id_bankomatu = @id_bankomatu
	--	PRINT N'Nie dodano rekordu, poniewaz id_oddzialu = ' + @id_oddzialu
	--end
	--else 
	--	PRINT N'Dodano rekord (' + RTRIM(CAST(@id_bankomatu AS varchar(20))) + ', ' + RTRIM(CAST(@id_oddzialu AS varchar(20))) + ')'
end 
