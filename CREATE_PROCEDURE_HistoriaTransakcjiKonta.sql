CREATE PROCEDURE HistoriaTransakcjiKonta
	@id_konta numeric(18, 0)
AS
BEGIN
	if exists (select 1 from sysobjects where id = object_id('TempTableOperacje'))
	drop table TempTableOperacje

	CREATE TABLE dbo.TempTableOperacje ( 
		typ_operacji char(24), 
		kwota float,
		data datetime);


	INSERT INTO dbo.TempTableOperacje (typ_operacji, kwota, data) select CAST('op. gotowkowa' as char(24)), og.kwota, og.data
	from Operacja_gotowkowa og
	where id_konta = @id_konta

	INSERT INTO dbo.TempTableOperacje (typ_operacji, kwota, data) select CAST('op. bezgotowkowa' as char(24)), ob.kwota, ob.data
	from Operacja_bezgotowkowa ob
	where id_nadawcy = @id_konta

	INSERT INTO dbo.TempTableOperacje (typ_operacji, kwota, data) select CAST('op. przewalutowania' as char(24)), op.kwota, op.data
	from Operacja_przewalutowania op
	where id_konta1 = @id_konta


	select * from dbo.TempTableOperacje 
END

if exists (select 1 from sysobjects where id = object_id('TempTableOperacje'))
drop table TempTableOperacje