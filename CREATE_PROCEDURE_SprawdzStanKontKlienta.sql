SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE SprawdzStanKontKlienta
	@id_klienta numeric(18, 0)	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT id_konta, stan, waluta 
	from Konto
	where id_klienta = @id_klienta
END
GO