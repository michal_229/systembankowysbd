create trigger Kurs_walut_aiu
on Kurs_walut
after insert, update 
as
begin
	declare @id_kursu numeric(18, 0) 
	declare @waluta1 char(3) 
	declare @waluta2 char(3) 
	declare @wartosc float

	select @id_kursu = id_kursu, 
		@waluta1 = waluta1,
		@waluta2 = waluta2,
		@wartosc = wartosc
	from inserted

	if (@wartosc <= 0) or ((1/@wartosc) <= 0) begin
		delete from Kurs_walut where id_kursu = @id_kursu
		PRINT N'niepoprawne dane: wartosc'
	end
	--else 
	--	PRINT N'wszystko spoko'
end 