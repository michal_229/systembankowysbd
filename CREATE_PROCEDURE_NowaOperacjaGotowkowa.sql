
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE NowaOperacjaGotowkowa
	-- Add the parameters for the stored procedure here
	@id_konta numeric(18, 0) = null,
	@id_bankomatu numeric(18, 0) = null,
	@typ_operacji bit = null,
	@kwota int = null
AS
BEGIN
	SET NOCOUNT ON;

	declare @data datetime = getdate()

	insert into Operacja_gotowkowa (id_konta, id_bankomatu, typ_operacji, kwota, data) values (@id_konta, @id_bankomatu, @typ_operacji, @kwota, @data)

    -- Insert statements for procedure here
	SELECT @id_konta, @id_bankomatu, @typ_operacji, @kwota, @data
END
GO
