
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE DodajKarte 
	-- Add the parameters for the stored procedure here
	@id_konta numeric(18, 0) = null, 
	@limit float = null,
	@pin smallint = 1234,
	@nr_karty int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	insert into Karta (id_konta, limit, pin, nr_karty) values (@id_konta, @limit, @pin, @nr_karty)

    -- Insert statements for procedure here
	SELECT @id_konta, @nr_karty
END
GO
