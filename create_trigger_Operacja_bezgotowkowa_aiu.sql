create trigger Operacja_bezgotowkowa_aiu
on Operacja_bezgotowkowa
after insert, update 
as
begin
	declare @id_operacji numeric(18, 0) 
	declare @id_nadawcy numeric(18, 0) 
	declare @id_odbiorcy numeric(18, 0) 
	declare @kwota float
	declare @data datetime

	declare @waluta_konta_nadawcy char(3)
	declare @waluta_konta_odbiorcy char(3)
	declare @stan_konta_nadawcy float
	declare @stan_konta_odbiorcy float
	declare @wartosc float = 1


	select @id_operacji = id_operacji, 
		@id_nadawcy = id_nadawcy,
		@id_odbiorcy = id_odbiorcy,
		@kwota = kwota,
		@data = data
	from inserted

	if (@kwota <= 0) or (@id_nadawcy = @id_odbiorcy) begin -- done: zabezpieczyc przed przelewaniem na to samo konto
		delete from Operacja_bezgotowkowa where id_operacji = @id_operacji
		PRINT N'niepoprawne dane: kwota <= 0 lub id_nadawcy = id_odbiorcy'
	end
	else begin
		-- info: id_nadawcy i id_odbiorcy nawiazuja do id_konta nadawcy i id_konta odbiorcy

		select @waluta_konta_nadawcy = waluta from Konto
		where id_konta = @id_nadawcy

		select @waluta_konta_odbiorcy = waluta from Konto
		where id_konta = @id_odbiorcy

		select @stan_konta_nadawcy = stan from Konto
		where id_konta = @id_nadawcy

		select @stan_konta_odbiorcy = stan from Konto
		where id_konta = @id_odbiorcy
	
		if (@waluta_konta_nadawcy = @waluta_konta_odbiorcy) begin -- lub zrobic @kwota_przewalutowana = @kwota * @wartosc niezaleznie od tego czy waluty sie zgadzaja czy nie, jesli sie zgadzaja to bedzie @wartosc = 1
			update Konto 
			set stan = @stan_konta_nadawcy - @kwota
			where id_konta = @id_nadawcy

			update Konto 
			set stan = @stan_konta_odbiorcy + @kwota
			where id_konta = @id_odbiorcy
		end 
		else begin -- if (waluty sie nie zgadzaja)
			-- done: przewalutowac kwote na walute konta odbiorcy -> kwota_przewalutowana : float
			select @wartosc = wartosc from Kurs_walut
			where waluta1 = @waluta_konta_nadawcy and waluta2 = @waluta_konta_odbiorcy
			 
			-- done: update stan konta nadawcy: stan = stan - kwota
			update Konto 
			set stan = @stan_konta_nadawcy - @kwota
			where id_konta = @id_nadawcy

			-- done: update stan konta odbiorcy: stan = stan + kwota_przewalutowana 
			update Konto 
			set stan = @stan_konta_odbiorcy + @kwota*@wartosc
			where id_konta = @id_odbiorcy
		end
		PRINT N'wszystko spoko'
	end
end 