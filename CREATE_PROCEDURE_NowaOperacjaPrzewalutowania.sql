
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE NowaOperacjaPrzewalutowania
	-- Add the parameters for the stored procedure here
	@id_konta1 numeric(18, 0) = null,
	@id_konta2 numeric(18, 0) = null,
	@kwota int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @data datetime = getdate()
	insert into Operacja_przewalutowania (id_konta1, id_konta2, kwota, data) values (@id_konta1, @id_konta2, @kwota, @data)

    -- Insert statements for procedure here
	SELECT @id_konta1, @id_konta2, @kwota, @data
END
GO
