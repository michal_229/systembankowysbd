create trigger Klient_aiu
on Klient
after insert, update 
as
begin
	-- nie da sie dobrac do pola nazwa_klienta : text

	declare @id_klienta numeric(18, 0) 
	declare @pesel numeric(18, 0) 

	select @id_klienta = id_klienta, 
		@pesel = pesel
	from inserted

	if @pesel < 1000000000 begin
		-- delete from Klient where id_klienta = @id_klienta
		-- usuwanie z Konto po pesel? czy zrobic to tylko w trigger_Dane_personalne_aiu
		PRINT N'niepoprawne dane: pesel'
	end
	--else 
	--	PRINT N'wszystko spoko'
end 