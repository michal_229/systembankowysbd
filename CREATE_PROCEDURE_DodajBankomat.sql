
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE DodajBankomat 
	-- Add the parameters for the stored procedure here
	@id_oddzialu numeric(18, 0), 
	@adres text
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	insert into Bankomat (id_oddzialu, adres) values (@id_oddzialu, @adres)



    -- Insert statements for procedure here
	 SELECT @id_oddzialu, @adres
END
GO
