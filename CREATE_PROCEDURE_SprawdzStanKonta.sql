SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE SprawdzStanKonta 
	@id_konta numeric(18, 0)	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT stan, waluta 
	from Konto
	where id_konta = @id_konta
END
GO
