create trigger Operacja_gotowkowa_aiu
on Operacja_gotowkowa
after insert, update 
as
begin
	declare @id_operacji numeric(18, 0) 
	declare @id_konta numeric(18, 0) 
	declare @id_bankomatu numeric(18, 0) 
	declare @typ_operacji bit
	declare @kwota int
	declare @data datetime

	declare @stan float -- poprzedni stan konta
	declare @oddzial_konta numeric(18, 0)
	declare @oddzial_bankomatu numeric(18, 0)

	select @id_operacji = id_operacji, 
		@id_konta = id_konta,
		@id_bankomatu = id_bankomatu,
		@typ_operacji = typ_operacji,
		@kwota = kwota,
		@data = data
	from inserted

	select @oddzial_konta = id_oddzialu from Konto
	where id_konta = @id_konta

	select @oddzial_bankomatu = id_oddzialu from Bankomat
	where id_bankomatu = @id_bankomatu

	-- done: sprawdzac, czy bankomat jest z tego samego oddzialu co konto klienta
	if (@kwota <= 0) or (@oddzial_konta <> @oddzial_bankomatu) begin
		delete from Operacja_gotowkowa where id_operacji = @id_operacji
		PRINT N'niepoprawne dane: kwota <= 0 lub oddzial_konta <> oddzial_bankomatu'
	end
	else begin
		select @stan = stan 
		from Konto
		where id_konta = @id_konta

		if @typ_operacji = 1 begin
		 	update Konto 
			set stan = @stan + @kwota
			where id_konta = @id_konta 
		end
		else begin
		 	update Konto 
			set stan = @stan - @kwota
			where id_konta = @id_konta
		end

		PRINT N'wszystko spoko'
	end
end 