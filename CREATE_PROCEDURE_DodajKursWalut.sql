
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE DodajKursWalut 
	-- Add the parameters for the stored procedure here
	@waluta1 char(3) = null, 
	@waluta2 char(3) = null,
	@wartosc float = 1 -- wartosc = waluta1/waluta2
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	insert into Kurs_walut (waluta1, waluta2, wartosc) values (@waluta1, @waluta2, @wartosc), (@waluta2, @waluta1, 1/@wartosc)

    -- Insert statements for procedure here
	SELECT @waluta1, @waluta2, @wartosc
END
GO
