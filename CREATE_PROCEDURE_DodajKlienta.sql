
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE DodajKlienta 
	-- Add the parameters for the stored procedure here
	@pesel numeric(18, 0) = null, 
	@nazwa_klienta text = null, 
	@imie char(20) = null, 
	@nazwisko char(30) = null, 
	@adres text = null, 
	@nr_telefonu char(12) = null, 
	@nr_dowodu char(9) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- todo: trzeba naprawic problem z PESEL
	-- insert into Dane_personalne (pesel, imie, nazwisko, adres, nr_telefonu, nr_dowodu) values (@pesel, @imie, @nazwisko, @adres, @nr_telefonu, @nr_dowodu)
	insert into Dane_personalne (imie, nazwisko, adres, nr_telefonu, nr_dowodu) values (@imie, @nazwisko, @adres, @nr_telefonu, @nr_dowodu)
	-- insert into Klient (pesel, nazwa_klienta) values (@pesel, @nazwa_klienta) -- poki co klient bedzie tworzony w triggerze Dane_personalne_aiu
	
    -- Insert statements for procedure here
	SELECT @nazwa_klienta
END
GO
