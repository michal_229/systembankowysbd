# README #

### OPIS ###

Projekt bazy danych na SBD - System Bankowy 
realizującego następujące funkcje:
1. Przelewy
2. Wpłaty/wypłaty
3. Sprawdzanie stanu konta
4. Historia transakcji
5. Wymiana waluty

Na zaliczenie maja wpływ:
1. Ocena sprawozdania laboratoryjnego
2. Ocena projektu zawartego w repozytorium na serwerze instytutowym.

Sprawozdanie laboratoryjne powinno zawierać:
1. Modele danych:
  a. Diagramy związków encji,
  b. Logiczny model danych,
  c. Fizyczny model danych
2. Elementy projektu bazy danych: perspektywy, więzy integralnościowe, indeksy, partycje, itp.
3. Opis logiki systemu zawarty w triggerach, funkcjach i procedurach PL/SQL.


### TODO ###

* naprawic baze (nie da sie wpisac PESEL), po naprawie odpowiednio dostosowac procedury insertu z nr PESEL
* dopracowac, co zwracaja procedury
* dodac wszedzie Bank.nazwa_tabeli
* sprecyzowac komunikaty bledow (ify jakies moze)