create trigger Dane_personalne_aiu
on Dane_personalne
after insert, update 
as
begin
	declare @pesel numeric(18, 0) 
	declare @imie char(20) 
	declare @nazwisko char(30) 
	declare @nr_telefonu char(12) 
	declare @nr_dowodu char(9) 
	declare @id_klienta numeric(18, 0)

	select @pesel = pesel, 
		@imie = imie,
		@nazwisko = nazwisko,
		@nr_telefonu = nr_telefonu,
		@nr_dowodu = nr_dowodu 
	from inserted

	if @nr_telefonu < 1000000 or @nr_dowodu < 100000 begin -- todo: warunek imienia i nazwiska
		PRINT N'niepoprawne dane: nr_telefonu lub nr_dowodu'

		-- usuwanie z Dane_personalne po pesel
		delete from Dane_personalne where pesel = @pesel
		-- usuwanie z Klient po pesel
		select @id_klienta = id_klienta from Klient where pesel = @pesel
		-- delete from Klient where pesel = @pesel -- jeszcze nie dodalem klienta, wiec nie ma czego usuwac
		-- usuwanie z Konto po id_klienta (otrzymane po pesel z Klient)
		delete from Konto where id_klienta = @id_klienta
	end
	else begin
		insert into Klient (pesel, nazwa_klienta) values (@pesel, @imie + @nazwisko) -- todo: ewentualnie przerobic @imie + @nazwisko na cos innego
		PRINT N'wszystko spoko'
	end
end 