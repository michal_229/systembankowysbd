create trigger Konto_aiu
on Konto
after insert, update 
as
begin
	declare @id_konta numeric(18, 0) 
	declare @id_oddzialu numeric(18, 0) 
	declare @id_klienta numeric(18, 0) 
	declare @stan float
	declare @waluta char(3)

	select @id_konta = id_konta, 
		@id_oddzialu = id_oddzialu,
		@id_klienta = id_klienta,
		@stan = stan,
		@waluta = waluta 
	from inserted

	if @stan < 0 begin
		PRINT N'klient jest zadluzony'
	end
	--else 
	--	PRINT N'wszystko spoko'
end 