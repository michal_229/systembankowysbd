create trigger Karta_aiu
on Karta
after insert, update 
as
begin
	-- nie da sie dobrac do pola adres : text

	declare @id_karty numeric(18, 0) 
	declare @id_konta numeric(18, 0) 
	declare @limit float
	declare @pin smallint
	declare @nr_karty int

	select @id_karty = id_karty, 
		@id_konta = id_konta,
		@limit = limit,
		@pin = pin,
		@nr_karty = nr_karty 
	from inserted

	if @limit < 0 or @nr_karty < 0 begin
		delete from Karta where id_karty = @id_karty
		PRINT N'niepoprawne dane: limit lub numer karty'
	end
	--else 
	--	PRINT N'wszystko spoko'
end 