create trigger Operacja_przewalutowania_aiu
on Operacja_przewalutowania
after insert, update 
as
begin
	declare @id_operacji numeric(18, 0) 
	declare @id_konta1 numeric(18, 0) 
	declare @id_konta2 numeric(18, 0) 
	declare @kwota float
	declare @data datetime

	declare @waluta_konta1 char(3)
	declare @waluta_konta2 char(3)
	declare @wartosc float = 1
	declare @stan_konta1 float
	declare @stan_konta2 float
	-- declare @kwota_przewalutowana float
	
	declare @id_klienta1 numeric(18, 1)
	declare @id_klienta2 numeric(18, 1)

	select @id_operacji = id_operacji, 
		@id_konta1 = id_konta1,
		@id_konta2 = id_konta2,
		@kwota = kwota,
		@data = data
	from inserted

	select @id_klienta1 = id_klienta from Konto
	where id_konta = @id_konta1

	select @id_klienta2 = id_klienta from Konto
	where id_konta = @id_konta2

	-- done: zabezpieczyc przed przelewaniem na to samo konto
	-- done: sprawdzac, czy oba konta naleza do tego samego klienta
	if (@kwota <= 0) or (@id_konta1 = @id_konta2) or (@id_klienta1 <> @id_klienta2) begin 
		delete from Operacja_przewalutowania where id_operacji = @id_operacji
		PRINT N'niepoprawne dane: kwota <= 0 lub id_konta1 = id_konta2 lub id_klienta1 <> id_klienta2'
	end
	else begin
		select @waluta_konta1 = waluta from Konto
		where id_konta = @id_konta1

		select @waluta_konta2 = waluta from Konto
		where id_konta = @id_konta2

		select @stan_konta1 = stan from Konto
		where id_konta = @id_konta1

		select @stan_konta2 = stan from Konto
		where id_konta = @id_konta2

		if (@waluta_konta1 <> @waluta_konta2) begin
			select @wartosc = wartosc from Kurs_walut
			where waluta1 = @waluta_konta1 and waluta2 = @waluta_konta2  

			-- done: update stan konta1: stan = stan - kwota
			update Konto 
			set stan = @stan_konta1 - @kwota
			where id_konta = @id_konta1

			-- @kwota_przewalutowana = @kwota * @wartosc
			-- done: update stan konta2: stan = stan + @kwota_przewalutowana 
			update Konto 
			set stan = @stan_konta2 + @kwota * @wartosc
			where id_konta = @id_konta2
		 end
		 else begin
			-- done: nic nie trzeba przewalutowywac
			PRINT N'nic nie trzeba przewalutowywac, waluta jest ta sama'
		 end
		PRINT N'wszystko spoko'
	end
end 