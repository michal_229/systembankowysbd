/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     2015-06-15 19:19:37                          */
/*==============================================================*/


if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Bankomat') and o.name = 'FK_BANKOMAT_BANKOMATY_ODDZIAL_')
alter table Bankomat
   drop constraint FK_BANKOMAT_BANKOMATY_ODDZIAL_
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Karta') and o.name = 'FK_KARTA_KARTY_KON_KONTO')
alter table Karta
   drop constraint FK_KARTA_KARTY_KON_KONTO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Klient') and o.name = 'FK_KLIENT_DANE_KLIE_DANE_PER')
alter table Klient
   drop constraint FK_KLIENT_DANE_KLIE_DANE_PER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Konto') and o.name = 'FK_KONTO_KONTA_KLI_KLIENT')
alter table Konto
   drop constraint FK_KONTO_KONTA_KLI_KLIENT
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Konto') and o.name = 'FK_KONTO_KONTA_ODD_ODDZIAL_')
alter table Konto
   drop constraint FK_KONTO_KONTA_ODD_ODDZIAL_
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Operacja_bezgotowkowa') and o.name = 'FK_OPERACJA_KONTO_NAD_KONTO')
alter table Operacja_bezgotowkowa
   drop constraint FK_OPERACJA_KONTO_NAD_KONTO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Operacja_bezgotowkowa') and o.name = 'FK_OPERACJA_KONTO_ODB_KONTO')
alter table Operacja_bezgotowkowa
   drop constraint FK_OPERACJA_KONTO_ODB_KONTO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Operacja_gotowkowa') and o.name = 'FK_OPERACJA_OPERACJE__BANKOMAT')
alter table Operacja_gotowkowa
   drop constraint FK_OPERACJA_OPERACJE__BANKOMAT
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Operacja_gotowkowa') and o.name = 'FK_OPERACJA_WPLATY_WY_KONTO')
alter table Operacja_gotowkowa
   drop constraint FK_OPERACJA_WPLATY_WY_KONTO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Operacja_przewalutowania') and o.name = 'FK_OPERACJA_KONTO_WAL_KONTO1')
alter table Operacja_przewalutowania
   drop constraint FK_OPERACJA_KONTO_WAL_KONTO1
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Operacja_przewalutowania') and o.name = 'FK_OPERACJA_KONTO_WAL_KONTO2')
alter table Operacja_przewalutowania
   drop constraint FK_OPERACJA_KONTO_WAL_KONTO2
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Bankomat')
            and   name  = 'Bankomaty_oddzialu_FK'
            and   indid > 0
            and   indid < 255)
   drop index Bankomat.Bankomaty_oddzialu_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Bankomat')
            and   type = 'U')
   drop table Bankomat
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Dane_personalne')
            and   type = 'U')
   drop table Dane_personalne
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Karta')
            and   name  = 'Karty_konta_FK'
            and   indid > 0
            and   indid < 255)
   drop index Karta.Karty_konta_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Karta')
            and   type = 'U')
   drop table Karta
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Klient')
            and   name  = 'Dane_klienta_FK'
            and   indid > 0
            and   indid < 255)
   drop index Klient.Dane_klienta_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Klient')
            and   type = 'U')
   drop table Klient
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Konto')
            and   name  = 'Konta_klienta_FK'
            and   indid > 0
            and   indid < 255)
   drop index Konto.Konta_klienta_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Konto')
            and   name  = 'Konta_oddzialu_FK'
            and   indid > 0
            and   indid < 255)
   drop index Konto.Konta_oddzialu_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Konto')
            and   type = 'U')
   drop table Konto
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Kurs_walut')
            and   type = 'U')
   drop table Kurs_walut
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Oddzial_banku')
            and   type = 'U')
   drop table Oddzial_banku
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Operacja_bezgotowkowa')
            and   name  = 'Konto_odbiorcy_FK'
            and   indid > 0
            and   indid < 255)
   drop index Operacja_bezgotowkowa.Konto_odbiorcy_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Operacja_bezgotowkowa')
            and   name  = 'Konto_nadawcy_FK'
            and   indid > 0
            and   indid < 255)
   drop index Operacja_bezgotowkowa.Konto_nadawcy_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Operacja_bezgotowkowa')
            and   type = 'U')
   drop table Operacja_bezgotowkowa
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Operacja_gotowkowa')
            and   name  = 'Wplaty_wyplaty_konta_FK'
            and   indid > 0
            and   indid < 255)
   drop index Operacja_gotowkowa.Wplaty_wyplaty_konta_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Operacja_gotowkowa')
            and   name  = 'Operacje_bankomatu_FK'
            and   indid > 0
            and   indid < 255)
   drop index Operacja_gotowkowa.Operacje_bankomatu_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Operacja_gotowkowa')
            and   type = 'U')
   drop table Operacja_gotowkowa
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Operacja_przewalutowania')
            and   name  = 'konto_waluty2_FK'
            and   indid > 0
            and   indid < 255)
   drop index Operacja_przewalutowania.konto_waluty2_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Operacja_przewalutowania')
            and   name  = 'Konto_waluty1_FK'
            and   indid > 0
            and   indid < 255)
   drop index Operacja_przewalutowania.Konto_waluty1_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Operacja_przewalutowania')
            and   type = 'U')
   drop table Operacja_przewalutowania
go

/*==============================================================*/
/* Table: Bankomat                                              */
/*==============================================================*/
create table Bankomat (
   id_bankomatu         numeric              identity,
   id_oddzialu          numeric              null,
   adres                text                 null,
   constraint PK_BANKOMAT primary key nonclustered (id_bankomatu)
)
go

/*==============================================================*/
/* Index: Bankomaty_oddzialu_FK                                 */
/*==============================================================*/
create index Bankomaty_oddzialu_FK on Bankomat (
id_oddzialu ASC
)
go

/*==============================================================*/
/* Table: Dane_personalne                                       */
/*==============================================================*/
create table Dane_personalne (
   pesel                numeric              identity,
   imie                 char(20)             not null,
   nazwisko             char(30)             not null,
   adres                text                 not null,
   nr_telefonu          char(12)             not null,
   nr_dowodu            char(9)              not null,
   constraint PK_DANE_PERSONALNE primary key nonclustered (pesel)
)
go

/*==============================================================*/
/* Table: Karta                                                 */
/*==============================================================*/
create table Karta (
   id_karty             numeric              identity,
   id_konta             numeric              null,
   limit                float                null,
   pin                  smallint             null,
   nr_karty             int                  null,
   constraint PK_KARTA primary key nonclustered (id_karty)
)
go

/*==============================================================*/
/* Index: Karty_konta_FK                                        */
/*==============================================================*/
create index Karty_konta_FK on Karta (
id_konta ASC
)
go

/*==============================================================*/
/* Table: Klient                                                */
/*==============================================================*/
create table Klient (
   id_klienta           numeric              identity,
   pesel                numeric              null,
   nazwa_klienta        text                 not null,
   constraint PK_KLIENT primary key nonclustered (id_klienta)
)
go

/*==============================================================*/
/* Index: Dane_klienta_FK                                       */
/*==============================================================*/
create index Dane_klienta_FK on Klient (
pesel ASC
)
go

/*==============================================================*/
/* Table: Konto                                                 */
/*==============================================================*/
create table Konto (
   id_konta             numeric              identity,
   id_oddzialu          numeric              not null,
   id_klienta           numeric              not null,
   stan                 float                not null,
   waluta               char(3)              not null,
   constraint PK_KONTO primary key nonclustered (id_konta)
)
go

/*==============================================================*/
/* Index: Konta_oddzialu_FK                                     */
/*==============================================================*/
create index Konta_oddzialu_FK on Konto (
id_oddzialu ASC
)
go

/*==============================================================*/
/* Index: Konta_klienta_FK                                      */
/*==============================================================*/
create index Konta_klienta_FK on Konto (
id_klienta ASC
)
go

/*==============================================================*/
/* Table: Kurs_walut                                            */
/*==============================================================*/
create table Kurs_walut (
   id_kursu             numeric              identity,
   waluta1              char(3)              not null,
   waluta2              char(3)              not null,
   wartosc              float                not null,
   constraint PK_KURS_WALUT primary key nonclustered (id_kursu)
)
go

/*==============================================================*/
/* Table: Oddzial_banku                                         */
/*==============================================================*/
create table Oddzial_banku (
   id_oddzialu          numeric              identity,
   adres                text                 not null,
   constraint PK_ODDZIAL_BANKU primary key nonclustered (id_oddzialu)
)
go

/*==============================================================*/
/* Table: Operacja_bezgotowkowa                                 */
/*==============================================================*/
create table Operacja_bezgotowkowa (
   id_operacji          numeric              identity,
   id_nadawcy           numeric              not null,
   id_odbiorcy          numeric              not null,
   kwota                float                not null,
   data                 datetime             not null,
   constraint PK_OPERACJA_BEZGOTOWKOWA primary key nonclustered (id_operacji)
)
go

/*==============================================================*/
/* Index: Konto_nadawcy_FK                                      */
/*==============================================================*/
create index Konto_nadawcy_FK on Operacja_bezgotowkowa (
id_nadawcy ASC
)
go

/*==============================================================*/
/* Index: Konto_odbiorcy_FK                                     */
/*==============================================================*/
create index Konto_odbiorcy_FK on Operacja_bezgotowkowa (
id_odbiorcy ASC
)
go

/*==============================================================*/
/* Table: Operacja_gotowkowa                                    */
/*==============================================================*/
create table Operacja_gotowkowa (
   id_operacji          numeric              identity,
   id_konta             numeric              null,
   id_bankomatu         numeric              null,
   typ_operacji         bit                  null,
   kwota                int                  null,
   data                 datetime             null,
   constraint PK_OPERACJA_GOTOWKOWA primary key nonclustered (id_operacji)
)
go

/*==============================================================*/
/* Index: Operacje_bankomatu_FK                                 */
/*==============================================================*/
create index Operacje_bankomatu_FK on Operacja_gotowkowa (
id_bankomatu ASC
)
go

/*==============================================================*/
/* Index: Wplaty_wyplaty_konta_FK                               */
/*==============================================================*/
create index Wplaty_wyplaty_konta_FK on Operacja_gotowkowa (
id_konta ASC
)
go

/*==============================================================*/
/* Table: Operacja_przewalutowania                              */
/*==============================================================*/
create table Operacja_przewalutowania (
   id_operacji          numeric              identity,
   id_konta1            numeric              null,
   id_konta2            numeric              null,
   kwota                int                  null,
   data                 datetime             null,
   constraint PK_OPERACJA_PRZEWALUTOWANIA primary key nonclustered (id_operacji)
)
go

/*==============================================================*/
/* Index: Konto_waluty1_FK                                      */
/*==============================================================*/
create index Konto_waluty1_FK on Operacja_przewalutowania (
id_konta1 ASC
)
go

/*==============================================================*/
/* Index: konto_waluty2_FK                                      */
/*==============================================================*/
create index konto_waluty2_FK on Operacja_przewalutowania (
id_konta2 ASC
)
go

alter table Bankomat
   add constraint FK_BANKOMAT_BANKOMATY_ODDZIAL_ foreign key (id_oddzialu)
      references Oddzial_banku (id_oddzialu)
go

alter table Karta
   add constraint FK_KARTA_KARTY_KON_KONTO foreign key (id_konta)
      references Konto (id_konta)
go

alter table Klient
   add constraint FK_KLIENT_DANE_KLIE_DANE_PER foreign key (pesel)
      references Dane_personalne (pesel)
go

alter table Konto
   add constraint FK_KONTO_KONTA_KLI_KLIENT foreign key (id_klienta)
      references Klient (id_klienta)
go

alter table Konto
   add constraint FK_KONTO_KONTA_ODD_ODDZIAL_ foreign key (id_oddzialu)
      references Oddzial_banku (id_oddzialu)
go

alter table Operacja_bezgotowkowa
   add constraint FK_OPERACJA_KONTO_NAD_KONTO foreign key (id_nadawcy)
      references Konto (id_konta)
go

alter table Operacja_bezgotowkowa
   add constraint FK_OPERACJA_KONTO_ODB_KONTO foreign key (id_odbiorcy)
      references Konto (id_konta)
go

alter table Operacja_gotowkowa
   add constraint FK_OPERACJA_OPERACJE__BANKOMAT foreign key (id_bankomatu)
      references Bankomat (id_bankomatu)
go

alter table Operacja_gotowkowa
   add constraint FK_OPERACJA_WPLATY_WY_KONTO foreign key (id_konta)
      references Konto (id_konta)
go

alter table Operacja_przewalutowania
   add constraint FK_OPERACJA_KONTO_WAL_KONTO1 foreign key (id_konta1)
      references Konto (id_konta)
go

alter table Operacja_przewalutowania
   add constraint FK_OPERACJA_KONTO_WAL_KONTO2 foreign key (id_konta2)
      references Konto (id_konta)
go

