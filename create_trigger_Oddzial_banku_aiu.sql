create trigger Oddzial_banku_aiu
on Oddzial_banku
after insert, update 
as
begin
	-- nie da sie dobrac do pola adres : text
	-- Msg 311, Level 16, State 1, Procedure Oddzial_banku_aiu, Line 7
	-- Cannot use text, ntext, or image columns in the 'inserted' and 'deleted' tables.
	-- mozna tylko sprawdzic, czy wszystko jest ok z id_oddzialu
	declare @id_oddzialu numeric(18, 0) 

	select @id_oddzialu = id_oddzialu from inserted

	-- if @id_oddzialu = 13 
	-- 	delete from Oddzial_banku where id_oddzialu = @id_oddzialu
end 
