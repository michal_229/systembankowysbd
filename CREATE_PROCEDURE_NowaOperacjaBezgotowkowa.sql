
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE NowaOperacjaBezgotowkowa 
	-- Add the parameters for the stored procedure here
	@id_nadawcy numeric(18, 0) = null, 
	@id_odbiorcy numeric(18, 0) = null, 
	@kwota numeric(18, 0) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @data datetime = GETDATE() -- wstawia aktualna date jako date transakcji
	insert into Operacja_bezgotowkowa (id_nadawcy, id_odbiorcy, kwota, data) values (@id_nadawcy, @id_odbiorcy, @kwota, @data)

    -- Insert statements for procedure here
	SELECT @id_nadawcy
END
GO
